authorization do
	role :admin do
		has_omnipotence
	end

	role :add do
		has_permission_on :threeddata, to: [:new, :create, :index, :show, :locate]
	end

	role :edit do
		has_permission_on :threeddata, to: [:edit, :update, :index, :show, :locate]
	end

	role :delete do
		has_permission_on :threeddata, to: [:delete, :destroy, :index, :show, :locate]
	end

	role :"only view" do
		has_permission_on :threeddata, to: [:index, :show, :locate]
	end

	role :"submit requisition" do
		has_permission_on :requisitions, to: [:index, :submit, :return]
	end

	role :"approve requisition" do
		has_permission_on :requisitions, to: [:index, :approve, :reject, :acknowledge]
	end
end