class AddColumnsToThreeddata < ActiveRecord::Migration
  def change
    add_column :threeddata, :submitter, :string
    add_column :threeddata, :approver, :string
  end
end
