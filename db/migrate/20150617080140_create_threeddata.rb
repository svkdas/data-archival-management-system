class CreateThreeddata < ActiveRecord::Migration
  def change
    create_table :threeddata do |t|

      t.string :tape_id
      t.string :area
      t.string :media_type
      t.datetime :aqdate
      t.string :clocation
      t.timestamps null: false
    end
  end
end
