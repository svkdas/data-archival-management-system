class AddIndexToThreeddata < ActiveRecord::Migration
  def change
  	add_reference :threeddata, :requisition, index:true
  end
end
