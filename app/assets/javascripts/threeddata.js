// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var loc=gon.cloc;							                 //retrieve data from database
var b;
var rowRackCoords=[];

rowRackCoords[0]=12+(300*(loc[0]-1));		       //advance number of rows

if (loc[0]%2==0) {							               //row is even number
	b=7-loc[1]+1;                                //row is complemnted of 7
} else {									
	b=loc[1];
}

rowRackCoords[0]+=41*(b-1);					          //advance number of racks
rowRackCoords[1]=8;							              //predefined

rowRackCoords[2]=rowRackCoords[0]+42;		      //second set of coordinates
rowRackCoords[3]=550;

var shelfBoxCoords=[];
var c;

shelfBoxCoords[0]=0;
shelfBoxCoords[1]=71*(loc[2]-1);			        //advance number of shelves
if (loc[3]<=4) {							                //front row
	c=loc[3];
	shelfBoxCoords[0]+=11;
	shelfBoxCoords[1]+=34;
	} else {								                    //back row
		c=loc[3]-4;
		shelfBoxCoords[0]+=29;
		shelfBoxCoords[1]+=6;
	}

shelfBoxCoords[0]+=40*(c-1);				          //advance number of boxes

shelfBoxCoords[2]=shelfBoxCoords[0]+15;		    //second set of coordinates
shelfBoxCoords[3]=shelfBoxCoords[1]+26;

$(document).ready(function () {
  $('#mainImage').mapster({
    singleSelect : false,
    mapKey: 'select',
    altImage: '/assets/pic2.png',
    areas : [{key : 'red1', selected : true}]
  });
  $('#mainImage2').mapster({
    singleSelect : false,
    mapKey: 'select',
    stroke: true,
    strokeColor: 'FF0000',
    strokeWidth: 5,
    fillColor : '00FF00',
    areas : [{key : 'red2', selected : true}]
  }).parent().css("opacity", "0.6").draggable();
  $('.margin').css("margin-left", "60px");
  $('#mainImage, #mainImage2').parent().css("position", "absolute");
});