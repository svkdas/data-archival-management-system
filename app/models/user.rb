class User < ActiveRecord::Base
  has_and_belongs_to_many :roles
  accepts_nested_attributes_for :roles
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :rememberable, :trackable, :validatable, 
  	:authentication_keys => [:cpfno]

  validates_presence_of :name
  validates_presence_of :cpfno

  scope :sorted, lambda { order("users.id ASC") }

  def email_required?
    false
  end

  def self.search_name(search_name)
    if search_name
      self.where("name LIKE ?", "%#{search_name}%")
    else
      self.all
    end
  end

  def self.search_cpfno(search_cpfno)
    if search_cpfno
      self.where("cpfno LIKE ?", "%#{search_cpfno}%")
    else
      self.all
    end
  end

  def role_symbols
    roles.map do |role|
      role.name.underscore.to_sym
    end
  end
end
