class Threeddatum < ActiveRecord::Base

	belongs_to :requisition

	validates_presence_of :tape_id
	validates_presence_of :area
	validates_presence_of :media_type
	validates_presence_of :clocation
	validates_uniqueness_of :tape_id
	validates_uniqueness_of :clocation, message: "Tapes already present in the given field"

	CLOC_REGEX = /\b[1-4][1-7][1-6][1-8]\b/

	validates_format_of :clocation, with: CLOC_REGEX

	def self.search_tapeid(search_tapeid)
		if search_tapeid
			self.where("tape_id LIKE ?", "%#{search_tapeid}%")
		else
			self.all
		end
	end
	def self.search_area(search_area)
		if search_area
			self.where("area LIKE ?", "%#{search_area}%")
		else
			self.all
		end
	end
	def self.search_mediatype(search_mediatype)
		if search_mediatype
			self.where("media_type LIKE ?", "%#{search_mediatype}%")
		else
			self.all
		end
	end
	def self.search_aqdate(search_aqdate)
		if search_aqdate
			self.where("aqdate LIKE ?", "%#{search_aqdate}%")
		else
			self.all
		end
	end
	def self.search_clocation(search_clocation)
		if search_clocation
			self.where("clocation LIKE ?", "%#{search_clocation}%")
		else
			self.all
		end
	end
end
