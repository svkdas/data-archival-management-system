class UsersController < ApplicationController
  filter_resource_access

  def index
    @users = User.sorted.paginate(page: params[:page], per_page: 10)
    @users = @users.search_name(params[:search_name]) if params[:search_name].present?
    @users = @users.search_cpfno(params[:search_cpfno]) if params[:search_cpfno].present?
  end

  def show
  end

  def edit
  end

  def update 
    if @user.update_attributes(user_params)
      flash[:notice] = "Role assigned successfully" 
      redirect_to(action: 'index', id: @user.id)
    else
      render('edit')
    end
  end


  def delete
  end

  def destroy
    @user.destroy
    flash[:notice] = "User #{@user.name} deleted form database"
    redirect_to(action: 'index')
  end

  private

  def user_params
    params.require(:user).permit(:name, :cpfno, role_ids: [])
  end

end
