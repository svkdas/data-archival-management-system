class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  before_action :configure_permitted_parameters, if: :devise_controller?

  before_filter { |c| Authorization.current_user = c.current_user }

  def configure_permitted_parameters
  	devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :cpfno, :password, :password_confirmation) }
  	devise_parameter_sanitizer.for(:account_update) << :name
  end

  def permission_denied
  	flash[:notice] = "You do not have the rights to view that page. Contact your supervisor."
  	redirect_to(root_url)
  end
end