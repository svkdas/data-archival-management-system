class ThreeddataController < ApplicationController
  filter_access_to :all

  def index
    @three_data = Threeddatum.all.paginate(page: params[:page], per_page: 5)
    @three_data = @three_data.search_tapeid(params[:search_tapeid]) if params[:search_tapeid].present?
    @three_data = @three_data.search_area(params[:search_area]) if params[:search_area].present?
    @three_data = @three_data.search_mediatype(params[:search_mediatype]) if params[:search_mediatype].present?
    @three_data = @three_data.search_aqdate(params[:search_aqdate]) if params[:search_aqdate].present?
    @three_data = @three_data.search_clocation(params[:search_clocation]) if params[:search_clocation].present?
  end

  def show
    @threeddatum = Threeddatum.find(params[:id])    
  end

  def new
  end

  def create
    @threeddatum = Threeddatum.new(threeddatum_params)
    if @threeddatum.save
      redirect_to(action: 'index')
    else
      render('new')
    end
  end

  def edit
    @threeddatum = Threeddatum.find(params[:id])
  end

  def update
    @threeddatum = Threeddatum.find(params[:id])
    if @threeddatum.update_attributes(threeddatum_params)
      flash[:notice] = "Updated Record successfully"
      redirect_to(action: 'index')
    else
      render('edit')
    end
  end

  def locate
    @threeddatum = Threeddatum.find(params[:id])
    gon.cloc = Threeddatum.find(params[:id]).clocation
  end

  def delete
    @threeddatum = Threeddatum.find(params[:id])
  end

  def destroy
    threeddatum = Threeddatum.find(params[:id]).destroy
    flash[:notice] = "Record #{threeddatum.tape_id} deleted from database"
    redirect_to(action: 'index')
  end

  private 
  def threeddatum_params
    params.require(:threeddatum).permit(:tape_id, :media_type, :aqdate, :area, :clocation)
  end

end