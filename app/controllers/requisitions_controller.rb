class RequisitionsController < ApplicationController
	filter_access_to :all
  def index
  	@data=Threeddatum.all.paginate(page: params[:page], per_page: 10)
    @data = @data.search_tapeid(params[:search_tapeid]) if params[:search_tapeid].present?
  end

  def submit
  	req=Requisition.find(2)
  	data=Threeddatum.find(params[:id])
  	data.submitter=current_user.name
  	req.threeddata << data
  	redirect_to(action: 'index')
  end

  def approve
  req=Requisition.find(3)
  data=Threeddatum.find(params[:id])
  data.approver=current_user.name
  req.threeddata << data
  redirect_to(action: 'index')
  end

  def reject
  	req=Requisition.find(1)
  	data=Threeddatum.find(params[:id])
  	req.threeddata << data
  	redirect_to(action: 'index')
  end

  def return
  	req=Requisition.find(4)
  	data=Threeddatum.find(params[:id])
  	if current_user.name == data.submitter
  		req.threeddata << data
  		redirect_to(action: 'index')
  	else
  		flash[:notice]="It is not yours to return"
  		redirect_to(action: 'index')
  	end
  end

  def acknowledge
  	req=Requisition.find(1)
  	data=Threeddatum.find(params[:id])
  	req.threeddata << data
  	redirect_to(action: 'index')
  end
end
